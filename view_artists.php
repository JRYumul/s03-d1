<?php
	include "header.php";
	include "navbar.php";
	include "db_connect.php";
?>

	<h1 class="my-4">View Artists</h1>
	<?php
		$sql = "SELECT * FROM artists";

		$result = mysqli_query($conn, $sql);

		if(mysqli_num_rows($result) > 0){
			// Loop through the records;
			while($row = mysqli_fetch_assoc($result)){
				echo $row["name"];
				echo "<br>";
			}
		}else{
			echo "No records";
		}
	?>

<?php
	include "footer.php";
?>