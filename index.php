<?php
	if(isset($_GET["message"])){
		$message = $_GET["message"];
		if($message == "1"){
			$message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Please log in<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			 	</button></div>';
		}else if($message == "2"){
			$message = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Invalid credentials<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  		 <span aria-hidden="true">&times;</span>
		  		</button></div>';
		}else if($message == "3"){
			$message = '<div class="alert alert-success alert-dismissible fade show" role="alert">' . "You've been logged out" . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
 				 </button></div>';
		}
	}else{
		$message = "";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PHP Programming</title>
	<!-- BOOTSTRAP CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	<section class="container">
		<h1 class="mt-5">Login Form</h1>
		<?php
			echo $message;
		?>
		<form method="POST" action="index_action.php">
			<div class="form-group">
				<span>Email address: </span><input class="form-control" type="email" placeholder="Enter email" name="uname"><br>
				<span>Password: </span><input class="form-control" type="password" placeholder="Enter password" name="pw"><br>
				<button class="btn btn-primary" type="submit">Submit</button>
				<button class="btn btn-primary" type="reset">Clear</button>
			</div>
		</form>
	</section>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>