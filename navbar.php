
<body>
	<div class="mb-5">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="home.php">Batch 54</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="home.php"><i class="fas fa-home mr-1"></i>Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about.php"><i class="fas fa-info-circle mr-1"></i>About</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart mr-1"></i>My Cart</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="add_artist.php"><i class="fas fa-user-plus mr-1"></i>Add Artist</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="view_artists.php"><i class="fas fa-users mr-1"></i>View Artists</a>
					</li>
				</ul>
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          <span class="text-white"><?php echo $_SESSION["username"]; ?></span>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt mr-1"></i>Log Out</a>
				        </div>
			     	</li>	
				</ul>
			</div>
		</nav>
	</div>
	<section class="container">