<?php
	include "header.php";
	include "navbar.php";
?>
		<h1 class="mb-5">My Cart</h1>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Item</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Sub-Total</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Watch</th>
                    <td><input type="number" value="1" min="0" class="qty"></td>
                    <td>P<span class="price">200.50</span></td>
                    <td>P<span class="sub"></span></td>
                    <td class="position-relative"><button class="btn btn-sm btn-outline-secondary text-dark position-absolute">Remove</button></td>
                </tr>
                <tr>
                    <th scope="row">Hat</th>
                    <td><input type="number" value="1" min="0" class="qty"></td>
                    <td>P<span class="price">35.50</span></td>
                    <td>P<span class="sub"></span></td>
                    <td class="position-relative"><button class="btn btn-sm btn-outline-secondary text-dark position-absolute">Remove</button></td>
                </tr>
            </tbody>
        </table>
        <h2 id="final">Total:</h2>
<?php
	include "footer.php";
?>