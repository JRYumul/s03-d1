let loc = window.location.href;
if(/home/.test(loc)){
    $("a[href='home.php']").addClass('active');
}else if(/about/.test(loc)){
    $("a[href='about.php']").addClass('active');
}else if(/cart/.test(loc)){
    $("a[href='cart.php']").addClass('active');
}else if(/add_artist/.test(loc)){
    $("a[href='add_artist.php']").addClass('active');
}else if(/view_artists/.test(loc)){
    $("a[href='view_artists.php']").addClass('active');
}

let qty = document.getElementsByClassName("qty");
let price = document.getElementsByClassName("price");
let sub = document.getElementsByClassName("sub");
let button = document.getElementsByClassName("btn");
let final  = document.getElementById("final");

function getTotals(){
    for(i = 0; i < qty.length; i++){
        let subtotal = parseFloat(qty[i].value) * parseFloat(price[i].innerHTML);
        sub[i].innerHTML = subtotal.toFixed(2);
    }
    let x = sub.length;
    let sum = 0;
    while(x--){
        if(qty[x].value == ""){
            sub[x].innerHTML = 0.00.toFixed(2);
            let total = sum += parseFloat(sub[x].innerHTML); 
            final.innerHTML = "Total: P" + total.toFixed(2);
        }else{
            let total = sum += parseFloat(sub[x].innerHTML);        
            final.innerHTML = "Total: P" + total.toFixed(2);
        }
    }
}

getTotals()

function removeItem(){
        this.parentNode.parentNode.remove();
        let x = sub.length;
        let sum = 0;
        if(x == 0){
            final.innerHTML = "Your cart is empty";
        }
        while(x--){
                let total = sum += parseFloat(sub[x].innerHTML);        
                final.innerHTML = "Total: P" + total.toFixed(2);
            }
}

for(let i = 0; i < qty.length; i++){
    qty[i].addEventListener("change", getTotals);
}

for(let i = 0; i < button.length; i++){
    button[i].addEventListener("click", removeItem);
}