<?php
	session_start();

	if(!isset($_SESSION["username"])){
		header("location: index.php?message=1");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PHP Programming</title>
	<!-- FONTAWESOME -->
	<script src="https://kit.fontawesome.com/acc6c30d64.js" crossorigin="anonymous"></script>
	<!-- BOOTSTRAP CSS -->
	<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
	<!-- EXTERNAL CSS -->
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
</head>